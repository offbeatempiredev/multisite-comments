=== Multisite Comment Manager ===
Contributors: jmdodd
Tags: admin, comments, manager, multisite
Requires at least: 3.1.4 
Tested up to: 3.2.1
Stable tag: 0.1

Provide multisite admins with a quick overview of recent comments on all sites.

== Description ==

Multisite Comment Manager is used in a multisite environment to allow admins to quickly 
overview comment behavior across all blogs for which they are users.

It is not suggested for use in multisite installations with a large number of sites because it
loops over each site for which a user has moderate_comments capabilities.

== Installation ==

1. Upload the directory `shadow-screen-options` and its contents to the `/wp-content/plugins/` directory.
1. Activate the plugin through the 'Plugins' menu in WordPress.

== Changelog ==

= 0.1 =
* Initial release. 

== Upgrade Notice ==

= 0.2 =
* Make the code more efficient.

= 0.1 = 
* Initial release.

== Credits ==

Development funded, in part, by Ariel Meadow Stallings and the Offbeat Empire.
